{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Integral with pair of coordinates and transformed coordinates at the same time\n",
    "\n",
    "Now that we know that\n",
    "\n",
    "$$ I = \\int dx \\, \\rho(x) \\, g(x) = \\int dx \\, \\rho(t(x)) \\, g(t(x)) \\, t^{\\prime}(x) $$\n",
    "\n",
    "we can use this information to write\n",
    "\n",
    "$$ I = \\int dx \\, \\frac{1}{2} \\, \\rho(x) \\left[ g(x) + \\frac{\\rho(t(x))}{\\rho(x)} \\, g(t(x)) \\, t^{\\prime}(x) \\right] $$\n",
    "\n",
    "In simple words, we can sample from the probability distribution function $\\rho(x)$ as usual, and then estimate an average of $g$ computed over the original and transformed coordinates (with some appropriate weights on the transformed part).\n",
    "\n",
    "In this notebook we demonstrate that this approach is correct with our usual example where we integrate in the range $[0, 2]$ with\n",
    "\n",
    "$$ \\rho(x) = \\frac{x}{2} $$\n",
    "\n",
    "$$ g(x) = 2 x $$\n",
    "\n",
    "$$ t(x) = 3 x^2 $$\n",
    "\n",
    "For simplicity, we will ignore autocorrelations when computing the standard error in the computation of the integral."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.express as px\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "import tensorflow_probability as tfp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the functions that enter in the integral and some other integral parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_warming_steps = 1000\n",
    "num_samples = 4000000\n",
    "\n",
    "g = lambda x: 2. * x\n",
    "transform = lambda x: 3. * x * x\n",
    "d1_transform = lambda x: 6. * x\n",
    "rho = tfp.distributions.Triangular(low=0.0, high=2.0, peak=2.).prob\n",
    "log_of_rho = tfp.distributions.Triangular(low=0.0, high=2.0, peak=2.).log_prob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use a simple Metropolis algorithm to sample from $\\rho$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [],
   "source": [
    "kernel = tfp.mcmc.RandomWalkMetropolis(target_log_prob_fn=log_of_rho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 3h 29min 15s, sys: 47.3 s, total: 3h 30min 2s\n",
      "Wall time: 3h 31min 52s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "samples = tfp.mcmc.sample_chain(\n",
    "  num_results=num_samples,\n",
    "  current_state=tf.ones(1),\n",
    "  kernel=kernel,\n",
    "  num_burnin_steps=num_warming_steps,\n",
    "  trace_fn=None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<tf.Tensor: shape=(3,), dtype=float32, numpy=array([0.8871658 , 0.8871658 , 0.39616683], dtype=float32)>"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples = tf.reshape(samples, [num_samples])\n",
    "samples[0:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integral computed using only the original coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates_values = g(samples)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Integral = 2.6668155193328857 +- 0.0004714063252322376\n"
     ]
    }
   ],
   "source": [
    "print(f'Integral = { tf.math.reduce_mean(coordinates_values) } '\n",
    "      f'+- { tf.math.reduce_std(coordinates_values)/np.sqrt(num_samples) }')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integral computed using only the transformed coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [],
   "source": [
    "transformed_values = [rho(transform(x)) * d1_transform(x) * g(transform(x))/ rho(x) for x in samples]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Integral = 2.673550605773926 +- 0.00418081833049655\n"
     ]
    }
   ],
   "source": [
    "print(f'Integral = { tf.math.reduce_mean(transformed_values) } '\n",
    "      f'+- { tf.math.reduce_std(transformed_values)/np.sqrt(num_samples) }')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integral computed using the average of the original and transformed coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [],
   "source": [
    "combined_values = [0.5 * (v + tv) for v, tv in zip(coordinates_values, transformed_values)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Integral = 2.670182943344116 +- 0.0020005106925964355\n"
     ]
    }
   ],
   "source": [
    "print(f'Integral = { tf.math.reduce_mean(combined_values) } '\n",
    "      f'+- { tf.math.reduce_std(combined_values)/np.sqrt(num_samples) }')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
