## Create a virtual environment

To install the python `requirements.txt`, you will need to specify your GitLab API token if you need to install the library `transformer-monte-carlo`.
If you don't have one already, you can obtain one starting from [here](https://docs.gitlab.com/ee/api/personal_access_tokens.html).

Export it with
```sh
export GITLAB_API_TOKEN="YOUR_TOKEN"
```

Afterwards, you can install all the requirements with the usual
```sh
pip install -r requirements.txt
```


## Sage notebooks

Some of the experiments in this repository are built on top of [sagemath](https://www.sagemath.org/).
If you find notebooks but no `requirements.txt` file, it is very likely the case.

We find convenient to run it in Docker. In the following the instructions.

Run the sage notebook (version 9.4) with Docker
```
docker container run -d --rm --name sage-notebook -p 8888:8888 -v $(pwd):/home/sage/notebooks sagemath/sagemath:9.4 sage-jupyter
```
Then execute
```
open $(docker container logs sage-notebook 2>&1 | grep 'http.*token' | tail -1 | sed -E 's/.*http(.*)token=(.*)/http:\/\/localhost:8888\/?token=\2/')
```
to open the jupyter notebook webpage.

Once finished, remember to stop the sage notebook Docker container with the command
```
docker container stop sage-notebook
```


---

**SAVE THE COMMANDS IN your `.*rc` FILE**

```
function start_sage {
  docker container run -d --rm --name sage-notebook -p 8888:8888 -v $(pwd):/home/sage/notebooks sagemath/sagemath:9.4 sage-jupyter
}

function open_sage {
  open $(docker container logs sage-notebook 2>&1 | grep 'http.*token' | tail -1 | sed -E 's/.*http(.*)token=(.*)/http:\/\/localhost:8888\/?token=\2/')
}

function stop_sage {
  ocker container stop sage-notebook
}
```

---
