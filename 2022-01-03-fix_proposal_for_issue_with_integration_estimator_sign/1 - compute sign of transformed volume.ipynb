{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sign of transformed volume\n",
    "\n",
    "The goal of this notebook is to illustrate a way to solve the issue described in the notebook `2021-04-18`.\n",
    "\n",
    "The idea is that the sign change can be observed by measuring the integration volume\n",
    "$$\n",
    "\\int dX .\n",
    "$$\n",
    "By definition, this should be positive, but when we apply a transformation and write\n",
    "$$\n",
    "\\int dX \\, {\\det(J_t)}(X) ,\n",
    "$$\n",
    "we may obtain a negative value. As already mentioned in the notebook `2021-04-18` this is a mistake due to the fact that the integration domain is $t^{-1}(\\Omega)$ and not $\\Omega$, but our simplified notation hid it.\n",
    "Computationally speaking, it is not clear how to integrate over $t^{-1}(\\Omega)$.\n",
    "\n",
    "Note that the same holds true for the integral\n",
    "$$\n",
    "\\int dX \\rho(X) ,\n",
    "$$\n",
    "which should always be equal to 1 (and be positive). However this integral has the advantage that we are computing the integral by sampling from $\\rho$. The advantage will become evident in the next paragraph.\n",
    "\n",
    "Proposal:\n",
    "$$\n",
    "I = \\int dX \\, \\rho(X) \\, \\left( \\alpha_0 \\, g(X) + \\alpha_1 \\, \\frac{\\rho(t(X))}{\\rho(X)} \\, g(t(X)) \\, {\\det(J_t)}(X) \\, \\mathcal{S}_t \\right)\n",
    "$$\n",
    "where\n",
    "$$\n",
    "\\mathcal{S}_t = \\text{sign}\\left( \\int dt(X) \\, \\rho(t(X)) \\right) = \\text{sign}\\left( \\int dX \\, \\rho(t(X)) \\, {\\det(J_t)}(X) \\right) = \\text{sign}\\left( \\int dX \\, \\rho(X) \\frac{\\rho(t(X))}{\\rho(X)} \\, {\\det(J_t)}(X) \\right)\n",
    "$$\n",
    "\n",
    "In practice this means that we can keep sampling from $\\rho$, and accumulate\n",
    "$$\n",
    "G = \\sum_i g(X_i)\n",
    "$$\n",
    "$$\n",
    "G^{\\prime} = \\sum_i \\frac{\\rho(t(X_i))}{\\rho(X_i)} \\, g(t(X_i)) \\, {\\det(J_t)}(X)\n",
    "$$\n",
    "$$\n",
    "S_t = \\sum_i \\frac{\\rho(t(X_i))}{\\rho(X_i)} \\, {\\det(J_t)}(X)\n",
    "$$\n",
    "Then\n",
    "$$\n",
    "I = \\alpha_0 \\, G + \\alpha_1 \\, S_t \\, G^{\\prime} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following we demonstrate that this approach works. In doing this we will set $\\alpha_0 = 0$ and $\\alpha_1 = 1$, and in practice we will compute only the term in front of $\\alpha_1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(x, y, z, q)"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    " var('x', 'y', 'z', 'q')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_3d_integral(a, b, f):\n",
    "    return integrate(integrate(integrate(f, x, a[0], b[0]), y, a[1], b[1]), z, a[2], b[2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_3d_sign(a, b, rho, t):\n",
    "    s = compute_3d_integral(a, b, rho(*t(x, y, z)) * jacobian(t(x, y, z), (x, y, z)).determinant())\n",
    "    return s/abs(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_3d_tmc_integral(a, b, rho, g, t):\n",
    "    S = compute_3d_sign(a, b, rho, t)\n",
    "    I = compute_3d_integral(a, b,\n",
    "                            rho(x, y, z)\n",
    "                            * (rho(*t(x, y, z)) / rho(x, y, z)) \n",
    "                            * g(*t(x, y, z)) \n",
    "                            * (jacobian(t(x, y, z), (x, y, z)).determinant()))\n",
    "    return S * I"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "rho_3d = lambda x, y, z: exp(-x^2-y^2-z^2) / sqrt(pi)\n",
    "g_3d = lambda x, y, z: cos(k * x + k * y + k * z)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pi*e^(-3/4)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t_3d = lambda x, y, z: (-x, -y, z)\n",
    "compute_3d_tmc_integral([-oo, -oo, -oo], [oo, oo, oo], rho_3d, g_3d, t_3d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pi*e^(-3/4)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t_3d = lambda x, y, z: (-x, y, z)\n",
    "compute_3d_tmc_integral([-oo, -oo, -oo], [oo, oo, oo], rho_3d, g_3d, t_3d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pi*e^(-3/4)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t_3d = lambda x, y, z: (-x, -y, -z)\n",
    "compute_3d_tmc_integral([-oo, -oo, -oo], [oo, oo, oo], rho_3d, g_3d, t_3d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pi*e^(-3/4)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t_3d = lambda x, y, z: (-x*x*x, -y, -z)\n",
    "compute_3d_tmc_integral([-oo, -oo, -oo], [oo, oo, oo], rho_3d, g_3d, t_3d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, the integral now has always the right sign and value."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.4",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
